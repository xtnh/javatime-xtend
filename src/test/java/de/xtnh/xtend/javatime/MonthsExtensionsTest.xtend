package de.xtnh.xtend.javatime

import java.time.LocalDate
import org.junit.Test

import static extension de.xtnh.xtend.javatime.MonthsExtensions.*
import static extension junit.framework.Assert.*


class MonthsExtensionsTest {
	
	@Test
	def operatorPlusMonths() {
		val date = LocalDate::of(2018, 9, 5)
		val nextDate = date + 2.months
		val expected = LocalDate::of(2018, 11, 5)
		expected.assertEquals(nextDate)
	}

	@Test
	def operatorMinusMonths() {
		val date = LocalDate::of(2018, 9, 5)
		val nextDate = date - 2.months
		val expected = LocalDate::of(2018, 7, 5)
		expected.assertEquals(nextDate)
	}
	
}