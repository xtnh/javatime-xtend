package de.xtnh.xtend.javatime;

import java.time.LocalDate

interface LocalDateExtensions extends DaysExtensions, MonthsExtensions {

	def static boolean operator_lessThan(LocalDate date1, LocalDate date2) {
		date1.isBefore(date2)
	}
	
	def static boolean operator_greaterThan(LocalDate date1, LocalDate date2) {
		date1.isAfter(date2)
	}
	
	def static LocalDate operator_plusPlus(LocalDate date) {
		date.plusDays(1)
	}
	
	def static LocalDate operator_minusMinus(LocalDate date) {
		date.minusDays(1)
	}
    
}
