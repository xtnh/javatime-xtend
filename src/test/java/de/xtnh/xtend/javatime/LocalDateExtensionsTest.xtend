package de.xtnh.xtend.javatime

import java.time.LocalDate
import org.junit.Test

import static extension de.xtnh.xtend.javatime.LocalDateExtensions.*
import static extension junit.framework.Assert.*

class LocalDateExtensionsTest {

	
	@Test
	def operatorPlusPlus() {
		val date = LocalDate::of(2018, 9, 5)
		val nextDate = date++
		val expected = LocalDate::of(2018, 9, 6)
		expected.assertEquals(nextDate)
	}
	
	@Test
	def operatorMinusMinus() {
		val date = LocalDate::of(2018, 9, 5)
		val nextDate = date--
		val expected = LocalDate::of(2018, 9, 4)
		expected.assertEquals(nextDate)
	}
	
	@Test
	def operatorLessThan() {
		val date1 = LocalDate::of(2018, 9, 5)
		assertTrue( date1 < (date1 + 1.days) )
		assertFalse( date1 < (date1 - 1.days) )
		assertFalse( date1 < date1 + 0.days )
	}
	
	@Test
	def operatorGreaterThan() {
		val date1 = LocalDate::of(2018, 9, 5)
		assertFalse( date1 > (date1 + 1.days) )
		assertTrue( date1 > (date1 - 1.days) )
		assertFalse( date1 > date1 + 0.days )
	}
	
}