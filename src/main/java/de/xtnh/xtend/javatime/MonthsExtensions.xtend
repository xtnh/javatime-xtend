package de.xtnh.xtend.javatime

import java.time.LocalDate

interface MonthsExtensions {

	def static Months months(int months) {
		Months::months(months)
	}

	def static LocalDate operator_plus(LocalDate date, Months months) {
		date.plusMonths(months.months)
	}
	
	def static LocalDate operator_minus(LocalDate date, Months months) {
		date.minusMonths(months.months)
	}

}
