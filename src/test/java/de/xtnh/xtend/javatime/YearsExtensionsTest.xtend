package de.xtnh.xtend.javatime

import java.time.LocalDate
import org.junit.Test

import static extension de.xtnh.xtend.javatime.YearsExtensions.*
import static extension junit.framework.Assert.*


class YearsExtensionsTest {
	
	@Test
	def operatorPlusYears() {
		val date = LocalDate::of(2018, 9, 5)
		val nextDate = date + 2.years
		val expected = LocalDate::of(2020, 9, 5)
		expected.assertEquals(nextDate)
	}

	@Test
	def operatorMinusYears() {
		val date = LocalDate::of(2018, 9, 5)
		val nextDate = date - 2.years
		val expected = LocalDate::of(2016, 9, 5)
		expected.assertEquals(nextDate)
	}
	
}