package de.xtnh.xtend.javatime;

final class Months {

	long months;

	private new(long months) {
		this.months = months
	}

	def getMonths() {
		months
	}

	def static months(long months) {
		new Months(months)
	}

}
