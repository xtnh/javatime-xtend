package de.xtnh.xtend.javatime

import java.time.LocalDate

interface YearsExtensions {

	def static Years years(int years) {
		Years::years(years)
	}

	def static LocalDate operator_plus(LocalDate date, Years years) {
		date.plusYears(years.years)
	}
	
	def static LocalDate operator_minus(LocalDate date, Years years) {
		date.minusYears(years.years)
	}

}
