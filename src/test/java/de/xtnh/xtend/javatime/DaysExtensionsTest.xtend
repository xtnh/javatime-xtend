package de.xtnh.xtend.javatime

import java.time.LocalDate
import org.junit.Test

import static extension de.xtnh.xtend.javatime.DaysExtensions.*
import static extension junit.framework.Assert.*

class DaysExtensionsTest {

	@Test
	def operatorPlusDays() {
		val date = LocalDate::of(2018, 9, 5)
		val nextDate = date + 2.days
		val expected = LocalDate::of(2018, 9, 7)
		expected.assertEquals(nextDate)
	}

	@Test
	def operatorMinusDays() {
		val date = LocalDate::of(2018, 9, 5)
		val nextDate = date - 2.days
		val expected = LocalDate::of(2018, 9, 3)
		expected.assertEquals(nextDate)
	}

}
