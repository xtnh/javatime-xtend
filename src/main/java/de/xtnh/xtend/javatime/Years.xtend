package de.xtnh.xtend.javatime;

final class Years {

	long years

	private new(long years) {
		this.years = years
	}

	def getYears() {
		years
	}

	def static years(long years) {
		new Years(years)
	}

}
