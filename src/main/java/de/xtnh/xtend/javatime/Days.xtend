package de.xtnh.xtend.javatime;

final class Days {

	long days

	private new(long days) {
		this.days = days
	}

	def getDays() {
		days
	}

	def static days(long days) {
		new Days(days)
	}

}
