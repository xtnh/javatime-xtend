package de.xtnh.xtend.javatime

import java.time.LocalDate

interface DaysExtensions {

	def static Days days(int days) {
		Days::days(days)
	}

	def static LocalDate operator_plus(LocalDate date, Days days) {
		date.plusDays(days.days)
	}
	
	def static LocalDate operator_minus(LocalDate date, Days days) {
		date.minusDays(days.days)
	}
	
}